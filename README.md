Your child’s successful education relies on a number of factors, one of the most important being consistency. Glarea’s Educational Continuity plan and program ensure your child will not miss a beat, whether participating at-home or in-school. Speak to us about the private school integration.

Address: 15000 - 54A Ave, Unit 201, Surrey, BC V3S 5X7, Canada

Phone: 604-372-3725

Website: [https://glareaschool.com](https://glareaschool.com)
